import { Module } from '@nestjs/common';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    UserModule,
    MongooseModule.forRoot(
      'mongodb+srv://haltoadmin:WzHZro892JqHHPv8hqMdSZGtus8p@cluster0.eioaj.mongodb.net/halto?retryWrites=true&w=majority',
    ),
  ],
  providers: [AppService],
})
export class AppModule {}

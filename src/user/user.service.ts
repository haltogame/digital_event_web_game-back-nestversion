import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User, UserDocument } from './schemas/user.schema';
import { generateToken } from 'src/helpers/token';
import * as sha256 from 'crypto-js/sha256';

export interface ApiResponse {
  success: boolean;
  message: string;
}

export interface CredToken {
  token: string;
}

export interface CredAuth {
  cleanedPseudo: string;
  password: string;
}

@Injectable()
export class UserService {
  constructor(
    @InjectModel(User.name) private readonly model: Model<UserDocument>,
  ) {}

  async findAll(): Promise<User[]> {
    try {
      return await this.model.find().exec();
    } catch (err) {
      throw new HttpException(
        `Error :  ${err.message}`,
        HttpStatus.BAD_REQUEST,
      );
    }
  }

  async findOne(id: string): Promise<User> {
    try {
      return await this.model.findById(id).exec();
    } catch (err) {
      throw new HttpException(
        `Error :  ${err.message}`,
        HttpStatus.BAD_REQUEST,
      );
    }
  }

  async create(createUserDto: CreateUserDto): Promise<User> {
    try {
      const hashedPassword = sha256(createUserDto.password).toString();
      createUserDto.password = hashedPassword;

      const token = generateToken();
      createUserDto.token = token;

      return await new this.model({
        ...createUserDto,
      }).save();
    } catch (err) {
      throw new HttpException(
        `Error :  ${err.message}`,
        HttpStatus.BAD_REQUEST,
      );
    }
  }

  async update(id: string, updateUserDto: UpdateUserDto): Promise<User> {
    try {
      return await this.model.findByIdAndUpdate(id, updateUserDto).exec();
    } catch (err) {
      throw new HttpException(
        `Error :  ${err.message}`,
        HttpStatus.BAD_REQUEST,
      );
    }
  }

  async delete(id: string): Promise<User> {
    try {
      return await this.model.findByIdAndDelete(id).exec();
    } catch (err) {
      throw new HttpException(
        `Error :  ${err.message}`,
        HttpStatus.BAD_REQUEST,
      );
    }
  }

  async loginWithToken(payload: CredToken): Promise<ApiResponse> {
    try {
      const user = await this.model.findOne({ token: payload.token }).exec();
      console.log(user)
      if(user) {
        return {success: true, message: "successfully login as"}
      } else {
        return {success: false, message: "Can't loggin"}
      }
    } catch (err) {
      throw new HttpException(
        `Error :  ${err.message}`,
        HttpStatus.BAD_REQUEST,
      );
    }
  }

  async loginWithAuth(payload: CredAuth): Promise<ApiResponse> {
    try {
      const newToken = generateToken()
      const user = await this.model.findOneAndUpdate({ cleanedPseudo: payload.cleanedPseudo, password: sha256(payload.password).toString() }, { token: newToken }).exec();
      console.log(user)
      if(user) {
        return {success: true, message: "successfully login as " + newToken}
      } else {
        return {success: false, message: "Can't loggin"}
      }
    } catch (err) {
      throw new HttpException(
        `Error :  ${err.message}`,
        HttpStatus.BAD_REQUEST,
      );
    }
  }
}

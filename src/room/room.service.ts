import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateRoomDto } from './dto/create-room.dto';
import { UpdateRoomDto } from './dto/update-room.dto';
import { Room, RoomDocument } from './schemas/room.schema';
import { generateToken } from 'src/helpers/token';
import * as sha256 from 'crypto-js/sha256';

export interface ApiResponse {
  success: boolean;
  message: string;
}

@Injectable()
export class RoomService {
  constructor(
    @InjectModel(Room.name) private readonly model: Model<RoomDocument>,
  ) {}

  async findAll(): Promise<Room[]> {
    try {
      return await this.model.find().exec();
    } catch (err) {
      throw new HttpException(
        `Error :  ${err.message}`,
        HttpStatus.BAD_REQUEST,
      );
    }
  }

  async findOne(id: string): Promise<Room> {
    try {
      return await this.model.findById(id).exec();
    } catch (err) {
      throw new HttpException(
        `Error :  ${err.message}`,
        HttpStatus.BAD_REQUEST,
      );
    }
  }

  async create(createRoomDto: CreateRoomDto): Promise<Room> {
    try {
      const hashedPassword = sha256(createRoomDto.password).toString();
      createRoomDto.password = hashedPassword;

      const token = generateToken();
      createRoomDto.token = token;

      return await new this.model({
        ...createRoomDto,
      }).save();
    } catch (err) {
      throw new HttpException(
        `Error :  ${err.message}`,
        HttpStatus.BAD_REQUEST,
      );
    }
  }

  async update(id: string, updateRoomDto: UpdateRoomDto): Promise<Room> {
    try {
      return await this.model.findByIdAndUpdate(id, updateRoomDto).exec();
    } catch (err) {
      throw new HttpException(
        `Error :  ${err.message}`,
        HttpStatus.BAD_REQUEST,
      );
    }
  }

  async delete(id: string): Promise<Room> {
    try {
      return await this.model.findByIdAndDelete(id).exec();
    } catch (err) {
      throw new HttpException(
        `Error :  ${err.message}`,
        HttpStatus.BAD_REQUEST,
      );
    }
  }
}

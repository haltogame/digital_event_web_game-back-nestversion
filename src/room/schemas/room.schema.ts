import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type RoomDocument = Room & Document;

@Schema()
export class Room {
  @Prop({ required: true, unique: true })
  cleanedPseudo: string;

  @Prop({ required: true })
  pseudo: string;

  @Prop({ required: true })
  password: string;

  @Prop({ required: true })
  score: number;

  @Prop({ required: true })
  token: string;
}

export const RoomSchema = SchemaFactory.createForClass(Room);

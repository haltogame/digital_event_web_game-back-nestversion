export class BaseRoomDto {
  cleanedPseudo: string;
  pseudo: string;
  password: string;
  score: 0;
  token: string;
}

import { BaseRoomDto } from './base-room.dto';

export class UpdateRoomDto extends BaseRoomDto {}
